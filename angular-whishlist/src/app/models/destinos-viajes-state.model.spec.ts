import {
  reducerDestinosViajes,
  DestinosViajesState,
  intializeDestinosViajesState,
  InitMyDataAction,
  NuevoDestinoAction,
  ElegidoFavoritoAction,
  VoteUpAction,
  VoteDownAction
} from './destinos-viajes-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
  it('should reduce init data', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(2);
    expect(newState.items[0].nombre).toEqual('destino 1');
  });

  it('should reduce new item added', () => {
    const prevState: DestinosViajesState = intializeDestinosViajesState();
    const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona', 'url'));
    const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
    expect(newState.items.length).toEqual(1);
    expect(newState.items[0].nombre).toEqual('barcelona');
  });

  it('should reduce select item', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
      let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      const action2: ElegidoFavoritoAction = new ElegidoFavoritoAction(newState.items[1]);
      newState = reducerDestinosViajes(newState, action2);
      expect(newState.items[1].isSelected()).toEqual(true);
    });

    it('should reduce voteUp item', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
      let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      let action2: VoteUpAction = new VoteUpAction(newState.items[1]);
      newState = reducerDestinosViajes(newState, action2);
      action2 = new VoteUpAction(newState.items[0]);
      newState = reducerDestinosViajes(newState, action2);
      expect(newState.items[0].votes).toEqual(1);
      expect(newState.items[1].votes).toEqual(1);
    });
    it('should reduce voteDown item', () => {
      const prevState: DestinosViajesState = intializeDestinosViajesState();
      const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
      let newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
      let action2: VoteDownAction = new VoteDownAction(newState.items[0]);
      newState = reducerDestinosViajes(newState, action2);
      action2 = new VoteDownAction(newState.items[0]);
      newState = reducerDestinosViajes(newState, action2);
      expect(newState.items[0].votes).toEqual(-2);
    });
});
