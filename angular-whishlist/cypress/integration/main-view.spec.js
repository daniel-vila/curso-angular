describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('Lista de Deseos!');
        cy.get('h1 b').should('contain', 'Ruteo simple');
    });

    it('.clear() - clears an input or textarea element', () => {
        // https://on.cypress.io/clear
        cy.get('[name="descripcion"]').type('Clear this text')
          .should('have.value', 'Clear this text')
          .clear()
          .should('have.value', '');
    });

    it('.submit() - submit a form', () => {
        // https://on.cypress.io/submit
        cy.get('form')
          .find('[type="text"]').type('Madrid')

        cy.get('form')
          .find('[name="descripcion"]').type('test con cypress')

        cy.get('form').submit();
    });
});